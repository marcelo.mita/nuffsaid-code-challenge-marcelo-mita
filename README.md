## React Coding Challenge

From this [repository](https://gitlab.com/hari22/nuffsaid-frontend-coding-challenge)

To execute:

`npm install` or `yarn`

Then:

`npm start` or `yarn start`

To run the tests

`npm test` or `yarn test`
