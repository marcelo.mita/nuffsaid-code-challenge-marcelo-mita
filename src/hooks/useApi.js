import { useState } from "react";

import Chance from "chance";
import lodash from "lodash";

const useApi = (messageCallback) => {
  const [isStarted, setIsStarted] = useState(true);
  const chance = new Chance();
  /**
   * priority from 1 to 3, 1 = error, 2 = warning, 3 = info
   * */
  const generate = () => {
    const message = chance.string();
    const priority = lodash.random(1, 3);
    const nextInMS = lodash.random(500, 3000);
    messageCallback({
      message,
      priority,
    });

    const id = setTimeout(() => {
      generate();
    }, nextInMS);
  };

  const stop = () => {
    setIsStarted(false);
    let id = setTimeout(() => {}, 0);
    while (id--) {
      clearTimeout(id);
    }
  };
  const start = () => {
    setIsStarted(true);
    generate();
  };

  return {
    start,
    stop,
    isStarted,
  };
};

export default useApi;
