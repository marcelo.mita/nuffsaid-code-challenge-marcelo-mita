import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Container,
  Grid,
  Snackbar,
  IconButton,
} from "@material-ui/core";

import CloseIcon from "@material-ui/icons/Close";
import useApi from "../../hooks/useApi";
import PriorityList from "../PriorityList";

const useStyles = makeStyles({
  commandButton: {
    backgroundColor: "#88FCA3",
    borderRadius: "5px",
    margin: "10px",
  },
  buttonsWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  errorAlert: {
    backgroundColor: "#F56236",
    borderRadius: "5px",
    "& > div": {
      backgroundColor: "transparent",
      color: "black",
    },
  },
  closeButton: {
    marginRight: "15px",
  },
});

const MessageList = () => {
  const classes = useStyles();
  const [messages, setMessages] = useState([]);
  const [open, setOpen] = useState(true);
  const [currentError, setCurrentError] = useState("");

  const messageCallback = (message) => {
    if (message.priority === 1) {
      setOpen(false);
      setCurrentError(message.message);
    }

    setMessages((prevMessages) => [message, ...prevMessages]);
  };

  const { start, stop, isStarted } = useApi(messageCallback);

  const toggleStart = () => {
    if (isStarted) {
      stop();
    } else {
      start();
    }
  };

  const clearAll = () => {
    setMessages([]);
  };

  const removeItemHandler = (message) => {
    const newMessages = messages.filter((m) => m.message !== message);
    setMessages(newMessages);
  };

  const handleAlertClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setCurrentError("");
  };

  useEffect(() => {
    start();
  }, []);

  useEffect(() => {
    setOpen(currentError !== "");
  }, [currentError]);

  return (
    <Container>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        open={open}
        autoHideDuration={2000}
        onClose={handleAlertClose}
        message={
          <React.Fragment>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleAlertClose}
              className={classes.closeButton}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
            {currentError}
          </React.Fragment>
        }
        className={classes.errorAlert}
        data-testid="snackbar"
      />
      <div className={classes.buttonsWrapper}>
        <Button
          variant="contained"
          onClick={toggleStart}
          className={classes.commandButton}
        >
          {isStarted ? "Stop" : "Start"}
        </Button>
        <Button
          variant="contained"
          onClick={clearAll}
          className={classes.commandButton}
        >
          Clear
        </Button>
      </div>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="flex-start"
        spacing={2}
      >
        <PriorityList
          messages={messages}
          priority={1}
          onRemoveItem={removeItemHandler}
        />
        <PriorityList
          messages={messages}
          priority={2}
          onRemoveItem={removeItemHandler}
        />
        <PriorityList
          messages={messages}
          priority={3}
          onRemoveItem={removeItemHandler}
        />
      </Grid>
    </Container>
  );
};

export default MessageList;
