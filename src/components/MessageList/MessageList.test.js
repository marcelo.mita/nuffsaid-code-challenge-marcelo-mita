import React from "react";
import { render, screen } from "@testing-library/react";

import MessageList from ".";

jest.mock("../../hooks/useApi", () => {
  const messages = require("../../utils/constants").messages;
  return jest.fn((callbackFunction) => ({
    start: () => {
      messages.forEach((msg) => {
        callbackFunction(msg);
      });
    },
    stop: () => {},
    isStarted: false,
  }));
});

test("should populate lists", () => {
  render(<MessageList />);
  expect(
    screen.getByRole("heading", {
      name: /error type 1/i,
    })
  ).toBeInTheDocument();
  expect(
    screen.getByRole("heading", {
      name: /warning type 2/i,
    })
  ).toBeInTheDocument();
  expect(
    screen.getByRole("heading", {
      name: /info type 3/i,
    })
  ).toBeInTheDocument();
  expect(screen.getAllByTestId("message-card")).toHaveLength(5);
  expect(screen.getByTestId("snackbar")).toBeInTheDocument();
});
