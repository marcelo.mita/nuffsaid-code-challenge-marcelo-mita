import React from "react";

import { render, screen } from "@testing-library/react";

import PriorityList from ".";

import { messages } from "../../utils/constants";

test("renders a list of error message cards", () => {
  render(<PriorityList messages={messages} priority={1} />);

  expect(
    screen.getByRole("heading", {
      name: /error type 1/i,
    })
  ).toBeInTheDocument();
  expect(screen.getByText(/count: 3/i)).toBeInTheDocument();
  expect(screen.getAllByTestId("message-card")).toHaveLength(3);
});

test("renders a list of warning message cards", () => {
  render(<PriorityList messages={messages} priority={2} />);

  expect(
    screen.getByRole("heading", {
      name: /warning type 2/i,
    })
  ).toBeInTheDocument();
  expect(screen.getByText(/count: 1/i)).toBeInTheDocument();
  expect(screen.getAllByTestId("message-card")).toHaveLength(1);
});

test("renders a list of info message cards", () => {
  render(<PriorityList messages={messages} priority={3} />);

  expect(
    screen.getByRole("heading", {
      name: /info type 3/i,
    })
  ).toBeInTheDocument();
  expect(screen.getByText(/count: 1/i)).toBeInTheDocument();
  expect(screen.getAllByTestId("message-card")).toHaveLength(1);
});
