import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";

import MessageCard from "../MessageCard";
import { priorities } from "../../utils/constants";

const useStyles = makeStyles({
  listTitle: {
    textTransform: "capitalize",
  },
});

const PriorityList = ({ messages, priority, onRemoveItem }) => {
  const classes = useStyles();
  const filteredMessages = messages.filter(
    (message) => message.priority === priority
  );

  return (
    <Grid item md={4}>
      <h1 className={classes.listTitle}>
        {priorities[priority]} Type {priority}
      </h1>
      <p>Count: {filteredMessages.length}</p>
      {filteredMessages.map((message) => (
        <MessageCard
          key={message.message}
          {...message}
          onClear={() => onRemoveItem(message.message)}
        />
      ))}
    </Grid>
  );
};

export default PriorityList;
