import React from "react";

import { priorities } from "../../utils/constants";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";

const useStyles = makeStyles({
  box: {
    borderRadius: "5px",
    padding: "16px",
    margin: "5px 0",
    height: "50px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  error: {
    backgroundColor: "#F56236",
  },
  warning: {
    backgroundColor: "#FCE788 ",
  },
  info: {
    backgroundColor: "#88FCA3",
  },
  clearButton: {
    textTransform: "none",
  },
});

const MessageCard = ({ message, priority, onClear }) => {
  const classes = useStyles();
  return (
    <div
      data-testid="message-card"
      className={`${classes.box} ${classes[priorities[priority]]}`}
    >
      <div>{message}</div>
      <Button className={classes.clearButton} onClick={onClear}>
        Clear
      </Button>
    </div>
  );
};

export default MessageCard;
