import React from "react";
import { render, screen } from "@testing-library/react";

import MessageCard from ".";

test("renders an error message card", () => {
  render(<MessageCard message="This is a test" priority={1} />);

  const el = screen.getByTestId("message-card");
  expect(el).toBeInTheDocument();
  expect(el.className).toMatch(/error/i);
  expect(el).toHaveTextContent("This is a test");
});

test("renders a warning message card", () => {
  render(<MessageCard message="This is a test" priority={2} />);

  const el = screen.getByTestId("message-card");
  expect(el).toBeInTheDocument();
  expect(el.className).toMatch(/warning/i);
  expect(el).toHaveTextContent("This is a test");
});

test("renders an info message card", () => {
  render(<MessageCard message="This is a test" priority={3} />);

  const el = screen.getByTestId("message-card");
  expect(el).toBeInTheDocument();
  expect(el.className).toMatch(/info/i);
  expect(el).toHaveTextContent("This is a test");
});
