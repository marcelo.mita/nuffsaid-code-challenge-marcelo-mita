export const priorities = { 1: "error", 2: "warning", 3: "info" };

export const messages = [
  {
    message: "test 1",
    priority: 1,
  },
  {
    message: "test 2",
    priority: 1,
  },
  {
    message: "test 3",
    priority: 1,
  },
  {
    message: "test 4",
    priority: 2,
  },
  {
    message: "test 5",
    priority: 3,
  },
];
